<?php
/*
 * the URL for the JSON post is
 * https://ha155.fusedesk.com/api/v1/cases/HJHFS4D9
 */

//if (isset($PropertyListId)){
    function fusedesk_api_data($method = "", $apikey = "", $url = "", $args = ""){
        $curl = curl_init();
        
//        $post_array = null;
//        if($method === 'POST') {
//            $post_array = "CURLOPT_POST => true, CURLOPT_POSTFIELDS =>" . $args;
//        }
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => "$url",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => false,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "$method",
//            $post_array,
//            CURLOPT_HTTPHEADER => array(
//                "X-FuseDesk-API-Key: $apikey",
//                "Content-Type: application/json"
//            ),
//        ));
        
        if($method === 'POST') {
            curl_setopt_array($curl, array(
                CURLOPT_URL => "$url",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "$method",
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $args,
                CURLOPT_HTTPHEADER => array(
                    "X-FuseDesk-API-Key: $apikey",
                    "Content-Type: application/json"
                ),
            ));
        }  else {
            $url = !empty($args) ? $url . '/?' . $args : $url;
            curl_setopt_array($curl, array(
                CURLOPT_URL => "$url",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "$method",
                CURLOPT_HTTPHEADER => array(
                    "X-FuseDesk-API-Key: $apikey",
                    "Content-Type: application/json"
                ),
            ));
        }
        
        $response = curl_exec($curl);
        $curl_response = json_decode($response, true);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
//            $response = "cURL Error #:" . $err;
            $response = array(
                'status' => 'error',
                'message' => $err
            );
        } else {
            $response = array(
                'status' => 'success',
                'message' => $curl_response
            );
        }
        return $response;
    }
//}