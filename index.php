<?php
    /**
     * Plugin Name:       FuseDesk API Endpoint
     * Plugin URI:        https://pixelomatic.com
     * Description:       FuseDesk Rest API Endpoint
     * Version:           1.0
     * Requires at least: 5.2
     * Requires PHP:      7.2
     * Author:            Pixelomatic
     * Author URI:        https://pixelomatic.com
     * License:           GPL v2 or later
     * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
     * Text Domain:       fusedesk-api-endpoint
 */

        require_once('api.php');

        function get_all_cases() {
            $app_name = 'ha155';
            $apikey = 'yZjeymjMr9AW2a2ZligHt0C0SyVIU3RZ';
            $caseid = 805622;

            // For Single Case ID
            $all_cases = fusedesk_api_data('GET', "$apikey", "https://$app_name.fusedesk.com/api/v1/cases/{$caseid}", "");

            // For All Case Id 
            // $all_cases = fusedesk_api_data('GET', "$apikey", "https://$app_name.fusedesk.com/api/v1/cases/", "");

                return $all_cases;
        }


        add_action('rest_api_init', function () {
            register_rest_route( 'wp/v1', 'getcases',array(
                        'methods'  => 'GET',
                        'callback' => 'get_all_cases'
                ));
        });